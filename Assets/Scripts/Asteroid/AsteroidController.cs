﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour
{
    public GameObject Explosion;
    public GameObject PlayerExplosion;

    public int DestructionPoints;

    private Rigidbody Rb;
    private ScoreManager ScoreManager;

    private void Start()
    {
        Rb = GetComponent<Rigidbody>();
        ScoreManager = ScoreManager.Find();
    }

    private void OnTriggerEnter(Collider other)
    {
        // Don't consider boundary
        if (other.tag == "Boundary")
        {
            return;
        }

        // Create an explosion
        Instantiate(Explosion, Rb.position, Rb.rotation);

        switch (other.tag)
        {
            // If destroyed by a shot, increment score
            case "Shot":
                ScoreManager.Score += DestructionPoints;
                break;

            // If other is the player, make it explode
            case "Player":
                var otherRb = other.GetComponent<Rigidbody>();
                Instantiate(PlayerExplosion, otherRb.position, otherRb.rotation);
                break;
        }

        // Destroy the other object
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
}
