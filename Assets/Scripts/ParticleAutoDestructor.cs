﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleAutoDestructor : MonoBehaviour
{
    private ParticleSystem ParticleSystem;

    // Start is called before the first frame update
    void Start()
    {
        ParticleSystem = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (ParticleSystem != null && !ParticleSystem.IsAlive())
        {
            Destroy(gameObject);
        }
    }
}
