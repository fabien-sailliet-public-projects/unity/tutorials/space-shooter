﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    
}

public class MoveLimiter : MonoBehaviour
{
    public float XMin;
    public float XMax;
    public float ZMin;
    public float ZMax;

    private void FixedUpdate()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.position = new Vector3
        (
            Mathf.Clamp(rb.position.x, XMin, XMax),
            0.0f,
            Mathf.Clamp(rb.position.z, ZMin, ZMax)
        );
    }
}
