﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HazardsSpawner : MonoBehaviour
{
    public GameObject Hazard;

    public Vector3 SpawnPositionsBound1;
    public Vector3 SpawnPositionsBound2;

    public float SpawnRate;
    public float SpawnDelay
    {
        get => 1 / SpawnRate;
    }

    private float LastSpawn;



    // Unity methods

    private void Start()
    {
        LastSpawn = Time.time;
    }

    private void Update()
    {
        if (Time.time - LastSpawn >= SpawnDelay)
        {
            LastSpawn = Time.time;

            SpawnAsteroid();
        }
    }



    // Private methods

    /// <summary>
    /// Spawn an asteroid at a random position
    /// </summary>
    private void SpawnAsteroid()
    {
        var spawnPosition = RandomVectorFromBounds(SpawnPositionsBound1, SpawnPositionsBound2);
        Instantiate(Hazard, spawnPosition, new Quaternion());
    }

    private Vector3 RandomVectorFromBounds(Vector3 vector1, Vector3 vector2)
    {
        return new Vector3(
            Random.Range(vector1.x, vector2.x),
            Random.Range(vector1.y, vector2.y),
            Random.Range(vector1.z, vector2.z)
        );
    }
}
