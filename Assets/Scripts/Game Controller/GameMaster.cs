﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMaster : MonoBehaviour
{
    public Text GameOverText;
    public Text RestartText;

    private ScoreManager ScoreManager;

    private bool _isGameOver = false;
    public bool IsGameOver
    {
        get => _isGameOver;
        set
        {
            _isGameOver = value;

            ScoreManager.IsPaused = value;
            GameOverText.enabled = value;
            RestartText.enabled = value;
        }
    }



    // Static Methods

    /// <summary>
    /// Find the instantiated GameMaster in the GameController (identified by tag)
    /// </summary>
    /// <returns>Instantiated ScoreManager</returns>
    public static GameMaster Find()
    {
        return GameObject.FindWithTag("GameController").GetComponent<GameMaster>();
    }



    // Unity Methods

    void Start()
    {
        ScoreManager = ScoreManager.Find();
    }

    private void Update()
    {
        // If game is over, allow to restart the game by pressing 'R'
        if (IsGameOver && Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
