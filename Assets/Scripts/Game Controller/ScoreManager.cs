﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text ScoreTextField;

    private int _score;
    public int Score
    {
        get => _score;
        set
        {
            // Ignore set if ScoreManager is paused
            if (!IsPaused)
            {
                _score = value;
                ScoreTextField.text = ScoreText + value;
            }
        }
    }

    private string ScoreText;
    public bool IsPaused { get; set; } = false;



    // Static Methods

    /// <summary>
    /// Find the instantiated ScoreManager in the GameController (identified by tag)
    /// </summary>
    /// <returns>Instantiated ScoreManager</returns>
    public static ScoreManager Find()
    {
        return GameObject.FindWithTag("GameController").GetComponent<ScoreManager>();
    }



    // Unity methods

    private void Start()
    {
        ScoreText = ScoreTextField.text;
        Score = 0;
    }
}
