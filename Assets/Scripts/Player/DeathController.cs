﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathController : MonoBehaviour
{
    private GameMaster GameMaster;

    private void Start()
    {
        GameMaster = GameMaster.Find();
    }

    private void OnDestroy()
    {
        GameMaster.IsGameOver = true;
    }
}
