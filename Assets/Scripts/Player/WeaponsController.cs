﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsController : MonoBehaviour
{
    public float FireRate;

    public GameObject Shot;
    public Transform ShotSpawn;

    public AudioSource ShotSound;

    private float LastFire;
    private float FireDelay
    {
        get
        {
            return 1 / FireRate;
        }
    }

    private void Start()
    {
        LastFire = Time.time - FireDelay;
    }

    private void Update()
    {
        if (Input.GetButton("Fire") && Time.time-LastFire >= FireDelay)
        {
            LastFire = Time.time;
            Instantiate(Shot, ShotSpawn.position, ShotSpawn.rotation);
            ShotSound.Play();
        }
    }
}
