﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllableMover : MonoBehaviour
{
    public float Speed;
    public float Tilt;

    private void FixedUpdate()
    {
        float horizMove = Input.GetAxis("Horizontal");
        float vertiMove = Input.GetAxis("Vertical");

        Rigidbody rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(horizMove, 0, vertiMove) * Speed;

        rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -Tilt);
    }
}
