﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundaryMalusesCounter : MonoBehaviour
{
    public int MissedAsteroidMalus;
    public int LostShotMalus;

    private ScoreManager ScoreManager;

    private void Start()
    {
        ScoreManager = ScoreManager.Find();
    }

    private void OnTriggerExit(Collider other)
    {
        // Apply maluses
        switch (other.tag)
        {
            case "Asteroid":
                ScoreManager.Score -= MissedAsteroidMalus;
                break;

            case "Shot":
                ScoreManager.Score -= LostShotMalus;
                break;
        }
    }
}
