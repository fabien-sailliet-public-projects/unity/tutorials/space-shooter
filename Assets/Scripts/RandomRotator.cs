﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotator : MonoBehaviour
{
    public float Tumble;

    private Rigidbody Rb;

    private void Start()
    {
        this.Rb = this.GetComponent<Rigidbody>();

        this.Rb.angularVelocity = Random.insideUnitSphere * this.Tumble;
    }
}
